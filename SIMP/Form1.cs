﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIMP
{
  public partial class Form1 : Form
  {
    byte[] test = { 1, 2, 3, 4, 5, 6, 7, 8 };
    PicSim picSim;
    byte[] theData = new byte[] { 14, 17, 5, 11, 2 };
    bool running = false;
    Label[] stackLabel = new Label[8];
    double passedTime = 0;


    public Form1()
    {

      InitializeComponent();
      picSim = new PicSim();
      picSim.init();
      byte pcltemp = 0;
      
      stackLabel[0] = labelS1;
      stackLabel[1] = labelS2;
      stackLabel[2] = labelS3;
      stackLabel[3] = labelS4;
      stackLabel[4] = labelS5;
      stackLabel[5] = labelS6;
      stackLabel[6] = labelS7;
      stackLabel[7] = labelS8;
      comboBox1.SelectedIndex = 3;
      UpdateMemory();



    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {

    }

    private void button1_Click(object sender, EventArgs e)
    {

      picSim.fetch();

      UpdateMemory();

    }

    private void cycle_Tick(object sender, EventArgs e)
    {

      picSim.fetch();
      UpdateMemory();
    }

    private void UpdateMemory()
    {
      cycleTimeCalc();
      dataGridView1.Rows.Clear();
      dataGridViewRA.Rows.Clear();
      dataGridViewRB.Rows.Clear();
      labelTimerZero.Text = picSim.memory[0, 1].ToString("X2");
      labelOption.Text = picSim.memory[1, 1].ToString("X2");
      labelLaufzeit.Text = String.Format("{0:0.##}", passedTime);

      for (int i = 0; i < dataGridView2.RowCount - 1; i++)
      {

        dataGridView2.Rows[i].Cells[1].Style.BackColor = Color.White;





      }
      for (int i = 0; i < 16; i++)
      {


        dataGridView1.Rows.Add(picSim.memory[0, (i * 8)].ToString("X2"), picSim.memory[0, (i * 8) + 1].ToString("X2"), picSim.memory[0, (i * 8) + 2].ToString("X2"), picSim.memory[0, (i * 8) + 3].ToString("X2"), picSim.memory[0, (i * 8) + 4].ToString("X2"), picSim.memory[0, (i * 8) + 5].ToString("X2"), picSim.memory[0, (i * 8) + 6].ToString("X2"), picSim.memory[0, (i * 8) + 7].ToString("X2"));

      }
      for (int i = 0; i < 16; i++)
      {


        dataGridView1.Rows.Add(picSim.memory[1, (i * 8)].ToString("X2"), picSim.memory[1, (i * 8) + 1].ToString("X2"), picSim.memory[1, (i * 8) + 2].ToString("X2"), picSim.memory[1, (i * 8) + 3].ToString("X2"), picSim.memory[1, (i * 8) + 4].ToString("X2"), picSim.memory[1, (i * 8) + 5].ToString("X2"), picSim.memory[1, (i * 8) + 6].ToString("X2"), picSim.memory[1, (i * 8) + 7].ToString("X2"));

      }


      for (int i = 0; i < dataGridView2.RowCount - 1; i++)
      {
        if (dataGridView2.Rows[i].Cells[2].Value.ToString() == picSim.memory[0, 2].ToString())
        {
          dataGridView2.Rows[i].Cells[1].Style.BackColor = Color.LightBlue;
          if (dataGridView2.Rows[i].Cells[0].Style.BackColor == Color.Red)
          {

            cycle.Enabled = false;
          }
        }

      }



      labelW.Text = picSim.wreg.ToString("X2");
      labelPCL.Text = picSim.memory[0, 2].ToString("X2");
      labelStatus.Text = picSim.memory[0, 3].ToString("X2");
      dataGridViewStatus.Rows.Clear();
      BitArray statusbits = new BitArray(System.BitConverter.GetBytes(picSim.memory[0, 3]));
      dataGridViewStatus.Rows.Add(statusbits.Get(7) ? "1" : "0", statusbits.Get(6) ? "1" : "0", statusbits.Get(5) ? "1" : "0", statusbits.Get(4) ? "1" : "0", statusbits.Get(3) ? "1" : "0", statusbits.Get(2) ? "1" : "0", statusbits.Get(1) ? "1" : "0", statusbits.Get(0) ? "1" : "0");
      for (int i = 0; i < 8; i++)
      {
        if (i < picSim.stack.Count)
        {
          stackLabel[i].Visible = true;
          stackLabel[i].Text = picSim.stack.ElementAt(i).ToString("X2");
        }
        else
        {
          stackLabel[i].Visible = false;
        }
      }
      dataGridViewOption.Rows.Clear();
      BitArray optionbits = new BitArray(System.BitConverter.GetBytes(picSim.memory[1, 1]));
      dataGridViewOption.Rows.Add(optionbits.Get(7) ? "1" : "0", optionbits.Get(6) ? "1" : "0", optionbits.Get(5) ? "1" : "0", optionbits.Get(4) ? "1" : "0", optionbits.Get(3) ? "1" : "0", optionbits.Get(2) ? "1" : "0", optionbits.Get(1) ? "1" : "0", optionbits.Get(0) ? "1" : "0");
      dataGridViewIntCon.Rows.Clear();
      BitArray intconbits = new BitArray(System.BitConverter.GetBytes(picSim.memory[0, 11]));
      dataGridViewIntCon.Rows.Add(intconbits.Get(7) ? "1" : "0", intconbits.Get(6) ? "1" : "0", intconbits.Get(5) ? "1" : "0", intconbits.Get(4) ? "1" : "0", intconbits.Get(3) ? "1" : "0", intconbits.Get(2) ? "1" : "0", intconbits.Get(1) ? "1" : "0", intconbits.Get(0) ? "1" : "0");

      BitArray portA = new BitArray(System.BitConverter.GetBytes(picSim.memory[0, 5]));
      BitArray trisA = new BitArray(System.BitConverter.GetBytes(picSim.memory[1, 5]));
      BitArray portB = new BitArray(System.BitConverter.GetBytes(picSim.memory[0, 6]));
      BitArray trisB = new BitArray(System.BitConverter.GetBytes(picSim.memory[1, 6]));
      dataGridViewRA.Rows.Add(trisA.Get(7) ? "I" : "O", trisA.Get(6) ? "I" : "O", trisA.Get(5) ? "I" : "O", trisA.Get(4) ? "I" : "O", trisA.Get(3) ? "I" : "O", trisA.Get(2) ? "I" : "O", trisA.Get(1) ? "I" : "O", trisA.Get(0) ? "I" : "O");
      dataGridViewRA.Rows.Add(portA.Get(7) ? "1" : "0", portA.Get(6) ? "1" : "0", portA.Get(5) ? "1" : "0", portA.Get(4) ? "1" : "0", portA.Get(3) ? "1" : "0", portA.Get(2) ? "1" : "0", portA.Get(1) ? "1" : "0", portA.Get(0) ? "1" : "0");
      dataGridViewRB.Rows.Add(trisB.Get(7) ? "I" : "O", trisB.Get(6) ? "I" : "O", trisB.Get(5) ? "I" : "O", trisB.Get(4) ? "I" : "O", trisB.Get(3) ? "I" : "O", trisB.Get(2) ? "I" : "O", trisB.Get(1) ? "I" : "O", trisB.Get(0) ? "I" : "O");
      dataGridViewRB.Rows.Add(portB.Get(7) ? "1" : "0", portB.Get(6) ? "1" : "0", portB.Get(5) ? "1" : "0", portB.Get(4) ? "1" : "0", portB.Get(3) ? "1" : "0", portB.Get(2) ? "1" : "0", portB.Get(1) ? "1" : "0", portB.Get(0) ? "1" : "0");

    }

    void cycleTimeCalc()
    {
      double cycleTime = 0;

      switch (comboBox1.SelectedIndex)
      {
        case 0:
          cycleTime = 31.25;
          cycle.Interval = 2000;
          break;
        case 1:
          cycleTime = 10;
          cycle.Interval = 1000;
          break;
        case 2:
          cycleTime = 2;
          cycle.Interval = 500;
          break;
        case 3:
          cycleTime = 1;
          cycle.Interval = 250;
          break;
        case 4:
          cycleTime = 0.5;
          cycle.Interval = 125;
          break;
        case 5:
          cycleTime = 0.25;
          cycle.Interval = 75;
          break;
        case 6:
          cycleTime = 0.125;
          cycle.Interval = 35;
          break;
        case 7:
          cycleTime = 0.0833;
          cycle.Interval = 20;
          break;
        case 8:
          cycleTime = 0.0625;
          cycle.Interval = 10;
          break;
        case 9:
          cycleTime = 0.05;
          cycle.Interval = 5;
          break;
        default:
          break;
      }
      passedTime += (picSim.cycles * cycleTime);
    }

    private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void Form1_Load(object sender, EventArgs e)
    {

    }

    private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void groupBox1_Enter(object sender, EventArgs e)
    {

    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void label2_Click(object sender, EventArgs e)
    {

    }

    private void label7_Click(object sender, EventArgs e)
    {

    }

    private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      try
      {

        int bank = 0;
        int rowIndex = e.RowIndex;
        if (e.RowIndex > 15)
        {
          bank = 1;
          rowIndex -= 16;
        }
        int cell = rowIndex * 8 + e.ColumnIndex;
        if (picSim != null)
        {
          string value = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
          picSim.memory[bank, cell] = Convert.ToByte(value, 16);
          UpdateMemory();
        }



      }
      catch (Exception ex)
      {

        Console.WriteLine(ex.Message);
      }


    }

    private void Resetbtn_Click(object sender, EventArgs e)
    {
      picSim.init();
      UpdateMemory();
    }

    private void Startbtn_Click(object sender, EventArgs e)
    {
      cycle.Enabled = true;

    }

    private void Stopbtn_Click(object sender, EventArgs e)
    {
      cycle.Enabled = false;
    }

    private void dataGridViewStatus_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
      {
        if (dataGridViewStatus.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "0")
        {
          dataGridViewStatus.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "1";
          picSim.setSpecificBit(3, (byte)switchindex(e.ColumnIndex), true);
        }
        else
        {
          dataGridViewStatus.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0";
          picSim.setSpecificBit(3, (byte)switchindex(e.ColumnIndex), false);
        }

        UpdateMemory();
      }

    }

    public byte switchindex(int index)
    {
      switch (index)
      {
        case 0:
          return 7;
        case 1:
          return 6;

        case 2:
          return 5;

        case 3:
          return 4;

        case 4:
          return 3;

        case 5:
          return 2;

        case 6:
          return 1;

        case 7:
          return 0;


        default:
          return 0;

      }
    }

    private void dataGridViewRA_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex >= 1 && e.ColumnIndex >= 0)
      {
        if (dataGridViewRA.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "0")
        {
          dataGridViewRA.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "1";
          picSim.setSpecificBit(5, (byte)switchindex(e.ColumnIndex), true);
        }
        else
        {
          dataGridViewRA.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0";
          picSim.setSpecificBit(5, (byte)switchindex(e.ColumnIndex), false);
        }

        UpdateMemory();
      }
    }

    private void dataGridViewRB_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.RowIndex >= 1 && e.ColumnIndex >= 0)
      {
        if (dataGridViewRB.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "0")
        {
          dataGridViewRB.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "1";
          picSim.setSpecificBit(6, (byte)switchindex(e.ColumnIndex), true);
        }
        else
        {
          dataGridViewRB.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "0";
          picSim.setSpecificBit(6, (byte)switchindex(e.ColumnIndex), false);
        }

        UpdateMemory();
      }
    }

    private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      if (e.ColumnIndex == 0)
      {
        if (dataGridView2.Rows[e.RowIndex].Cells[0].Style.BackColor == Color.Red)
        {

          dataGridView2.Rows[e.RowIndex].Cells[0].Style.BackColor = Color.White;
        }
        else
        {


          dataGridView2.Rows[e.RowIndex].Cells[0].Style.BackColor = Color.Red;
        }
      }

    }

    private void dataGridViewStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void buttonFileSelect_Click(object sender, EventArgs e)
    {
      openFileDialog1.ShowDialog();
      string filename = openFileDialog1.FileName;
      picSim.fetcher.setprogrammbuffer(filename);
      byte pcltemp = 0;
      dataGridView2.Rows.Clear();
      picSim.init();
      UpdateMemory();
      for (int i = 0; i < picSim.fetcher.file.Length; i++)
      {
        if (picSim.fetcher.file[i].StartsWith(" "))
        {
          dataGridView2.Rows.Add("", picSim.fetcher.file[i], "none");
        }
        else
        {
          dataGridView2.Rows.Add("", picSim.fetcher.file[i], pcltemp++);
        }
      }
    }
  }
}
