﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Numerics;

namespace SIMP
{
  // nachschauen wegen setzten status in mehreren bänken



  class PicSim
  {

    public Fetcher fetcher = new Fetcher();
    public byte[,] memory = new byte[2, 128];
    byte tmr0 = 0x1;
    byte pcl = 0x2;
    byte status = 0x3;
    byte porta = 0x5;
    byte portb = 0x6;
    public Stack<Int16> stack = new Stack<short>();

    public byte wreg { get; set; }
    public int cycles = 1;
    public int timerDelay = 0;

    public void init()
    {
      //Array.Clear(memory, 0, memory.Length);
      setMemoryValue(pcl, 0);
      wreg = 0;
      memory[1, 5] = 255;
      memory[1, 6] = 255;
      setMemoryValue(status, 0x18);
      setMemoryValue(0xB, 0);
      memory[1, 1] = 0xFF;
      stack.Clear();


    }

    public void fetch()
    {

      Int16 command = (short)fetcher.getcommand(memory[0, pcl]);
      BitArray bitarray;
      bitarray = new BitArray(System.BitConverter.GetBytes(command));
      memory[0, pcl]++;
      memory[1, pcl]++;
      decode(bitarray);
      checkforTimer();
      checkforInterrupt();

    }

    void decode(BitArray command)
    {
      bool desti = command.Get(7);
      cycles = 1;


      if (command.Get(13) & command.Get(12) & !command.Get(11) & !command.Get(10))
      {
        //MOVLW
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)255));
        command = command.And(mask);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        wreg = fileAddr[0];
      }
      else if (command.Get(13) & command.Get(12) & command.Get(11) & command.Get(10) & command.Get(9))
      {
        //ADDLW
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)255));
        command = command.And(mask);
        byte[] literal = new byte[2];
        command.CopyTo(literal, 0);
        int realresult = literal[0] + wreg;
        setDigitCarry(literal[0], wreg);
        wreg = (byte)(literal[0] + wreg);

        if (wreg == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        if (realresult > 255)
        {
          setCarryFlag(true);
        }
        else
        {
          setCarryFlag(false);
        }

      }
      else if (command.Get(13) & command.Get(12) & command.Get(11) & command.Get(10) & !command.Get(9))
      {
        //SUBLW
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)255));
        command = command.And(mask);
        byte[] literal = new byte[2];
        command.CopyTo(literal, 0);
        setDigitCarry(literal[0], (byte)(~wreg + 1));
        wreg = (byte)(literal[0] - wreg);
        if (wreg >= 0)
        {
          setCarryFlag(true);
          if (wreg == 0)
          {
            setZeroFlag(true);
          }
          else
          {
            setZeroFlag(false);
          }

        }
        else
        {
          setCarryFlag(false);
        }

      }
      else if (command.Get(13) & command.Get(12) & command.Get(11) & !command.Get(10) & !command.Get(9) & command.Get(8))
      {
        //ANDLW
        //needs test
        byte[] literal = new byte[2];
        command.CopyTo(literal, 0);
        wreg = (byte)(wreg & literal[0]);

        if (wreg == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }

      }
      else if (command.Get(13) & command.Get(12) & command.Get(11) & !command.Get(10) & !command.Get(9) & !command.Get(8))
      {
        //IORLW
        //needs test
        byte[] literal = new byte[2];
        command.CopyTo(literal, 0);
        wreg = (byte)(wreg | literal[0]);
        if (wreg == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
      }
      else if (command.Get(13) & command.Get(12) & command.Get(11) & !command.Get(10) & command.Get(9) & !command.Get(8))
      {
        //XORLW
        
        byte[] literal = new byte[2];
        command.CopyTo(literal, 0);
        wreg = (byte)(wreg ^ literal[0]);
        if (wreg == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
      }
      else if (command.Get(13) & !command.Get(12) & !command.Get(11))
      {
        //CALL
        cycles = 2;
        command.Set(13, false);
        byte[] jumpAddr = new byte[2];
        command.CopyTo(jumpAddr, 0);
        stack.Push(memory[0, pcl]);
        if (stack.Count == 9)
        {
          PushoutofStack();

        }
        memory[0, pcl] = jumpAddr[0];
        memory[1, pcl] = jumpAddr[0];


      }
      else if (command.Get(13) & !command.Get(12) & command.Get(11))
      {
        //GOTO
        cycles = 2;
        command.Set(13, false);
        byte[] jumpAddr = new byte[2];
        command.CopyTo(jumpAddr, 0);
        memory[0, pcl] = jumpAddr[0];
        memory[1, pcl] = jumpAddr[0];
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & !command.Get(9) & !command.Get(8) & command.Get(7))
      {
        //MOVWF
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);

        setMemoryValue(fileAddr[0], wreg);

      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & !command.Get(10) & !command.Get(9) & !command.Get(8))
      {
        //MOVF
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
       
        if (!command.Get(7))
        {

          wreg = getMemoryValue(fileAddr[0]);
        }
        command.Set(7, false);
        command.CopyTo(fileAddr, 0);
        if (getMemoryValue(fileAddr[0]) == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & command.Get(10) & command.Get(9) & command.Get(8))
      {
        //ADDWF
        //needs test
        command.Set(9, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        int realresult = file + wreg;
        byte result = (byte)(file + wreg);
        setDigitCarry(file, wreg);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        if (realresult > 255)
        {
          setCarryFlag(true);
        }
        else
        {
          setCarryFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);

      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & command.Get(10) & !command.Get(9) & command.Get(8))
      {
        //ANDWF
        //needs test
        command.Set(9, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file & wreg);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);

      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & command.Get(10) & !command.Get(9) & !command.Get(8))
      {
        //IORWF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file | wreg);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & command.Get(10) & command.Get(9) & !command.Get(8))
      {
        //XORWF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file ^ wreg);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & command.Get(10) & command.Get(9) & !command.Get(8))
      {
        //SWAPF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)((file & 0x0F) << 4 | (file & 0xF0) >> 4);
        checkdesti(desti, fileAddr[0], result);

      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & !command.Get(9) & command.Get(8) & command.Get(7))
      {
        //CLRF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        setZeroFlag(true);
        setMemoryValue(fileAddr[0], 0);
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & !command.Get(9) & command.Get(8) & !command.Get(7))
      {
        //CLRW
        //needs test
        wreg = 0;
        setZeroFlag(true);
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & !command.Get(10) & !command.Get(9) & command.Get(8))
      {
        //COMF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)~file;
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & command.Get(9) & !command.Get(8))
      {
        //SUBWF

        command.Set(9, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        setDigitCarry(file, (byte)(~wreg + 1));
        byte result = (byte)(file - wreg);
        int realresult = file - wreg;

        if (realresult >= 0)
        {
          setCarryFlag(true);
          if (result == 0)
          {
            setZeroFlag(true);
          }
          else
          {
            setZeroFlag(false);
          }
        }
        else
        {
          setCarryFlag(false);
        }
        checkdesti(desti, fileAddr[0], result);
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & command.Get(9) & command.Get(8))
      {
        //DECF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file - 1);
        checkdesti(desti, fileAddr[0], result);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & !command.Get(10) & command.Get(9) & command.Get(8))
      {
        //DECFSZ

        command.Set(11, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file - 1);
        checkdesti(desti, fileAddr[0], result);

        if (result == 0)
        {
          memory[0, pcl]++;
          memory[1, pcl]++;
          cycles = 2;
        }
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & !command.Get(10) & command.Get(9) & !command.Get(8))
      {
        //INCF
        //needs test
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file + 1);
        checkdesti(desti, fileAddr[0], result);
        if (result == 0)
        {
          setZeroFlag(true);
        }
        else
        {
          setZeroFlag(false);
        }
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & command.Get(10) & command.Get(9) & command.Get(8))
      {
        //INCFSZ

        command.Set(11, false);
        command.Set(10, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        byte result = (byte)(file + 1);
        checkdesti(desti, fileAddr[0], result);
        if (result == 0)
        {
          memory[0, pcl]++;
          memory[1, pcl]++;
          cycles = 2;
        }
      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & command.Get(10) & !command.Get(9) & command.Get(8))
      {
        //RLF

        int carry = new BitArray(System.BitConverter.GetBytes(memory[0, status])).Get(0) ? 1 : 0;

        command.Set(11, false);
        command.Set(10, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        bool newcarry = file > 127 ? true : false;
        byte result = (byte)(file * 2);
        result = carry == 1 ? (byte)(result + 1) : result;
        setSpecificBit(status, 0, newcarry);
        checkdesti(desti, fileAddr[0], result);


      }
      else if (!command.Get(13) & !command.Get(12) & command.Get(11) & command.Get(10) & !command.Get(9) & !command.Get(8))
      {
        //RRF

        int carry = new BitArray(System.BitConverter.GetBytes(memory[0, status])).Get(0) ? 1 : 0;

        command.Set(11, false);
        command.Set(10, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        command.CopyTo(fileAddr, 0);
        byte file = getMemoryValue(fileAddr[0]);
        bool newcarry = (file % 2 != 0) ? true : false;
        byte result = (byte)(file / 2);
        result = carry == 1 ? (byte)(result + 128) : result;
        setSpecificBit(status, 0, newcarry);
        checkdesti(desti, fileAddr[0], result);


      }
      else if (!command.Get(13) & command.Get(12) & !command.Get(11) & command.Get(10))
      {
        //BSF
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)896));
        mask = mask.And(command);
        mask.RightShift(7);
        command.Set(12, false);
        command.Set(10, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        byte[] help2 = new byte[2];
        command.CopyTo(fileAddr, 0);
        mask.CopyTo(help2, 0);
        setSpecificBit(fileAddr[0], help2[0], true);

      }
      else if (!command.Get(13) & command.Get(12) & !command.Get(11) & !command.Get(10))
      {
        //BCF
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)896));
        mask = mask.And(command);
        mask.RightShift(7);
        command.Set(12, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        byte[] help2 = new byte[2];
        command.CopyTo(fileAddr, 0);
        mask.CopyTo(help2, 0);
        setSpecificBit(fileAddr[0], help2[0], false);

      }
      else if (!command.Get(13) & command.Get(12) & command.Get(11) & !command.Get(10))
      {
        //BTFSC
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)896));
        mask = mask.And(command);
        mask.RightShift(7);
        command.Set(12, false);
        command.Set(11, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        byte[] help2 = new byte[2];
        command.CopyTo(fileAddr, 0);
        mask.CopyTo(help2, 0);
        BitArray memoryValue = new BitArray(System.BitConverter.GetBytes(getMemoryValue(fileAddr[0])));

        if (memoryValue.Get(help2[0]) == false)
        {
          memory[0, pcl]++;
          memory[1, pcl]++;
          cycles = 2;
        }
      }
      else if (!command.Get(13) & command.Get(12) & command.Get(11) & command.Get(10))
      {
        //BTFSS
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)896));
        mask = mask.And(command);
        mask.RightShift(7);
        command.Set(12, false);
        command.Set(11, false);
        command.Set(10, false);
        command.Set(9, false);
        command.Set(8, false);
        command.Set(7, false);
        byte[] fileAddr = new byte[2];
        byte[] help2 = new byte[2];
        command.CopyTo(fileAddr, 0);
        mask.CopyTo(help2, 0);
        BitArray memoryValue = new BitArray(System.BitConverter.GetBytes(getMemoryValue(fileAddr[0])));

        if (memoryValue.Get(help2[0]) == true)
        {
          memory[0, pcl]++;
          memory[1, pcl]++;
          cycles = 2;
        }
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & !command.Get(9) & !command.Get(8) & !command.Get(7) & !command.Get(6) & !command.Get(5) & !command.Get(4) & command.Get(3) & !command.Get(2) & !command.Get(1) & !command.Get(0))
      {
        //Return
        cycles = 2;
        setMemoryValue(pcl, (byte)stack.Pop());
      }
      else if (!command.Get(13) & !command.Get(12) & !command.Get(11) & !command.Get(10) & !command.Get(9) & !command.Get(8) & !command.Get(7) & !command.Get(6) & !command.Get(5) & !command.Get(4) & command.Get(3) & !command.Get(2) & !command.Get(1) & command.Get(0))
      {
        //ReturnFIE
        cycles = 2;
        setSpecificBit(0x0B, 7, true);
        setMemoryValue(pcl, (byte)stack.Pop());
      }
      else if (command.Get(13) & command.Get(12) & !command.Get(11) & command.Get(10))
      {
        //RetLW
        cycles = 2;
        BitArray mask;
        mask = new BitArray(System.BitConverter.GetBytes((Int16)255));
        mask = mask.And(command);
        byte[] help2 = new byte[2];
        mask.CopyTo(help2, 0);
        wreg = help2[0];
        setMemoryValue(pcl, (byte)stack.Pop());
      }



    }

    void setMemoryValue(byte Adresse, byte Value)
    {
      int bank = new BitArray(System.BitConverter.GetBytes(memory[0, status])).Get(5) ? 1 : 0;
      if (Adresse == 0 || Adresse == 2 || Adresse == 3 || Adresse == 4 || Adresse == 10 || Adresse == 11)
      {
        if (Adresse == 0)
        {
          
          if (memory[bank, 4] != 0)
          {
            setMemoryValue(memory[bank, 4], Value);
          }

        }
        else
        {
          memory[0, Adresse] = Value;
          memory[1, Adresse] = Value;
        }

      }
      else if (Adresse == 1 && bank == 0)
      {
        timerDelay = 2;
        memory[bank, Adresse] = Value;
      }
      else
      {
        memory[bank, Adresse] = Value;
      }

    }
    byte getMemoryValue(byte adresse)
    {
      int bank = new BitArray(System.BitConverter.GetBytes(memory[0, status])).Get(5) ? 1 : 0;
      if (adresse == 0)
      {
        if (memory[bank, 4] == 0)
        {
          return 0;
        }
        else
        {
          return getMemoryValue(memory[bank, 4]);
        }

      }

      return memory[bank, adresse];
    }


    public void setSpecificBit(byte adresse, byte bit, bool value)
    {

      byte[] fileAddr = new byte[2];
      BitArray memoryValue = new BitArray(System.BitConverter.GetBytes(getMemoryValue(adresse)));
      memoryValue.Set(bit, value);
      memoryValue.CopyTo(fileAddr, 0);
      setMemoryValue(adresse, fileAddr[0]);

    }

    void setCarryFlag(bool carry)
    {
      setSpecificBit(status, 0, carry);
    }

    void setZeroFlag(bool zero)
    {
      setSpecificBit(status, 2, zero);
    }

    void checkdesti(bool desti, byte adress, byte result)
    {
      if (desti)
      {
        setMemoryValue(adress, result);


      }
      else
      {
        wreg = result;
      }
    }
    void setDigitCarry(byte Arg1, byte Arg2)
    {
      Arg1 = (byte)(Arg1 & 0x0F);
      Arg2 = (byte)(Arg2 & 0x0F);

      if (Arg1 + Arg2 > 15)
      {
        setSpecificBit(status, 1, true);
      }
      else
      {
        setSpecificBit(status, 1, false);
      }

    }


    bool getSpecificBit(byte adresse, byte bank, byte bit)
    {


      byte value = memory[bank, adresse];
      byte temp = (byte)Math.Pow(2, bit);
      temp = (byte)(temp & value);
      if (temp == 0)
      {
        return false;
      }
      return true;


    }
    void checkforTimer()
    {
      if (!getSpecificBit(1, 1, 5))
      {
        if (timerDelay == 0)
        {
          memory[0, 1] += (byte)cycles;
        }
        else
        {
          if(timerDelay == 1 && cycles == 2)
          {
            timerDelay--;
            memory[0, 1]++;

          }
          else
          {
            timerDelay = 0;
          }
          
        }
        if (memory[0, 1] == 0 || ((cycles==2)&& memory[0, 1] == 1))
        {
          setSpecificBit(0x0B, 2, true);

        }
      }


    }

    void checkforInterrupt()
    {
      if (getSpecificBit(0x0B, 0, 5) && getSpecificBit(0x0B, 0, 7))
      {

        if (getSpecificBit(0x0B, 0, 2))
        {
          setSpecificBit(0x0B, 7, false);
          stack.Push(memory[0, pcl]);
          if (stack.Count == 9)
          {
            PushoutofStack();

          }
          setMemoryValue(pcl, 4);
        }
      }
    }

    void PushoutofStack()
    {
      Stack<Int16> temp = new Stack<short>();
      while (stack.Count != 0)
      {
        temp.Push(stack.Pop());
      }
      temp.Pop();
      while (temp.Count != 0)
      {
        stack.Push(temp.Pop());
      }
    }
  }
}
