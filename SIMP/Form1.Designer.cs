﻿
namespace SIMP
{
  partial class Form1
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.StartSimulation = new System.Windows.Forms.Button();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.cycle = new System.Windows.Forms.Timer(this.components);
      this.dataGridView2 = new System.Windows.Forms.DataGridView();
      this.Breakpoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Header = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PCL = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.labelTimerZero = new System.Windows.Forms.Label();
      this.labelVorteiler = new System.Windows.Forms.Label();
      this.labelOption = new System.Windows.Forms.Label();
      this.labelFSR = new System.Windows.Forms.Label();
      this.labelStatus = new System.Windows.Forms.Label();
      this.labelPCInt = new System.Windows.Forms.Label();
      this.labelPCLATH = new System.Windows.Forms.Label();
      this.labelPCL = new System.Windows.Forms.Label();
      this.labelW = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label18 = new System.Windows.Forms.Label();
      this.label17 = new System.Windows.Forms.Label();
      this.dataGridViewIntCon = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewOption = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.label10 = new System.Windows.Forms.Label();
      this.dataGridViewStatus = new System.Windows.Forms.DataGridView();
      this.ColumnIRP = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnRP1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnRP0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnT0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnPD = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnZero = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnDC = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ColumnC = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.labelS8 = new System.Windows.Forms.Label();
      this.labelS7 = new System.Windows.Forms.Label();
      this.labelS6 = new System.Windows.Forms.Label();
      this.labelS5 = new System.Windows.Forms.Label();
      this.labelS4 = new System.Windows.Forms.Label();
      this.labelS3 = new System.Windows.Forms.Label();
      this.labelS2 = new System.Windows.Forms.Label();
      this.labelS1 = new System.Windows.Forms.Label();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.Stopbtn = new System.Windows.Forms.Button();
      this.Startbtn = new System.Windows.Forms.Button();
      this.Resetbtn = new System.Windows.Forms.Button();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.label16 = new System.Windows.Forms.Label();
      this.label15 = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.label13 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.dataGridViewRB = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewRA = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.label19 = new System.Windows.Forms.Label();
      this.labelLaufzeit = new System.Windows.Forms.Label();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.label21 = new System.Windows.Forms.Label();
      this.Timing = new System.Windows.Forms.GroupBox();
      this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
      this.buttonFileSelect = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntCon)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOption)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatus)).BeginInit();
      this.groupBox3.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox5.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRA)).BeginInit();
      this.Timing.SuspendLayout();
      this.SuspendLayout();
      // 
      // StartSimulation
      // 
      this.StartSimulation.Location = new System.Drawing.Point(12, 89);
      this.StartSimulation.Name = "StartSimulation";
      this.StartSimulation.Size = new System.Drawing.Size(105, 23);
      this.StartSimulation.TabIndex = 2;
      this.StartSimulation.Text = "Einzelschritt";
      this.StartSimulation.UseVisualStyleBackColor = true;
      this.StartSimulation.Click += new System.EventHandler(this.button1_Click);
      // 
      // dataGridView1
      // 
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
      this.dataGridView1.Location = new System.Drawing.Point(1308, 25);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.RowHeadersWidth = 24;
      this.dataGridView1.RowTemplate.Height = 25;
      this.dataGridView1.Size = new System.Drawing.Size(434, 878);
      this.dataGridView1.TabIndex = 2;
      this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
      // 
      // Column1
      // 
      this.Column1.FillWeight = 50F;
      this.Column1.HeaderText = "00";
      this.Column1.Name = "Column1";
      this.Column1.Width = 50;
      // 
      // Column2
      // 
      this.Column2.HeaderText = "01";
      this.Column2.Name = "Column2";
      this.Column2.Width = 50;
      // 
      // Column3
      // 
      this.Column3.HeaderText = "02";
      this.Column3.Name = "Column3";
      this.Column3.Width = 50;
      // 
      // Column4
      // 
      this.Column4.HeaderText = "03";
      this.Column4.Name = "Column4";
      this.Column4.Width = 50;
      // 
      // Column5
      // 
      this.Column5.HeaderText = "04";
      this.Column5.Name = "Column5";
      this.Column5.Width = 50;
      // 
      // Column6
      // 
      this.Column6.HeaderText = "05";
      this.Column6.Name = "Column6";
      this.Column6.Width = 50;
      // 
      // Column7
      // 
      this.Column7.HeaderText = "06";
      this.Column7.Name = "Column7";
      this.Column7.Width = 50;
      // 
      // Column8
      // 
      this.Column8.HeaderText = "07";
      this.Column8.Name = "Column8";
      this.Column8.Width = 50;
      // 
      // cycle
      // 
      this.cycle.Interval = 500;
      this.cycle.Tick += new System.EventHandler(this.cycle_Tick);
      // 
      // dataGridView2
      // 
      this.dataGridView2.AllowUserToAddRows = false;
      this.dataGridView2.AllowUserToDeleteRows = false;
      this.dataGridView2.AllowUserToResizeColumns = false;
      this.dataGridView2.AllowUserToResizeRows = false;
      this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Breakpoint,
            this.Header,
            this.PCL});
      this.dataGridView2.Location = new System.Drawing.Point(12, 287);
      this.dataGridView2.MultiSelect = false;
      this.dataGridView2.Name = "dataGridView2";
      this.dataGridView2.ReadOnly = true;
      this.dataGridView2.RowTemplate.Height = 25;
      this.dataGridView2.Size = new System.Drawing.Size(936, 469);
      this.dataGridView2.TabIndex = 3;
      this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
      this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
      // 
      // Breakpoint
      // 
      this.Breakpoint.HeaderText = "BP";
      this.Breakpoint.Name = "Breakpoint";
      this.Breakpoint.ReadOnly = true;
      this.Breakpoint.Width = 30;
      // 
      // Header
      // 
      this.Header.HeaderText = "Programm";
      this.Header.Name = "Header";
      this.Header.ReadOnly = true;
      this.Header.Width = 1000;
      // 
      // PCL
      // 
      this.PCL.HeaderText = "Column9";
      this.PCL.Name = "PCL";
      this.PCL.ReadOnly = true;
      this.PCL.Visible = false;
      this.PCL.Width = 10;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.labelTimerZero);
      this.groupBox1.Controls.Add(this.labelVorteiler);
      this.groupBox1.Controls.Add(this.labelOption);
      this.groupBox1.Controls.Add(this.labelFSR);
      this.groupBox1.Controls.Add(this.labelStatus);
      this.groupBox1.Controls.Add(this.labelPCInt);
      this.groupBox1.Controls.Add(this.labelPCLATH);
      this.groupBox1.Controls.Add(this.labelPCL);
      this.groupBox1.Controls.Add(this.labelW);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.label8);
      this.groupBox1.Controls.Add(this.label7);
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Location = new System.Drawing.Point(12, 24);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(404, 257);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "SFR + W:";
      this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
      // 
      // labelTimerZero
      // 
      this.labelTimerZero.AutoSize = true;
      this.labelTimerZero.Location = new System.Drawing.Point(338, 105);
      this.labelTimerZero.Name = "labelTimerZero";
      this.labelTimerZero.Size = new System.Drawing.Size(44, 15);
      this.labelTimerZero.TabIndex = 17;
      this.labelTimerZero.Text = "label18";
      // 
      // labelVorteiler
      // 
      this.labelVorteiler.AutoSize = true;
      this.labelVorteiler.Location = new System.Drawing.Point(338, 70);
      this.labelVorteiler.Name = "labelVorteiler";
      this.labelVorteiler.Size = new System.Drawing.Size(44, 15);
      this.labelVorteiler.TabIndex = 16;
      this.labelVorteiler.Text = "label17";
      // 
      // labelOption
      // 
      this.labelOption.AutoSize = true;
      this.labelOption.Location = new System.Drawing.Point(338, 35);
      this.labelOption.Name = "labelOption";
      this.labelOption.Size = new System.Drawing.Size(44, 15);
      this.labelOption.TabIndex = 15;
      this.labelOption.Text = "label16";
      // 
      // labelFSR
      // 
      this.labelFSR.AutoSize = true;
      this.labelFSR.Location = new System.Drawing.Point(113, 210);
      this.labelFSR.Name = "labelFSR";
      this.labelFSR.Size = new System.Drawing.Size(44, 15);
      this.labelFSR.TabIndex = 14;
      this.labelFSR.Text = "label15";
      // 
      // labelStatus
      // 
      this.labelStatus.AutoSize = true;
      this.labelStatus.Location = new System.Drawing.Point(113, 175);
      this.labelStatus.Name = "labelStatus";
      this.labelStatus.Size = new System.Drawing.Size(44, 15);
      this.labelStatus.TabIndex = 13;
      this.labelStatus.Text = "label14";
      // 
      // labelPCInt
      // 
      this.labelPCInt.AutoSize = true;
      this.labelPCInt.Location = new System.Drawing.Point(113, 140);
      this.labelPCInt.Name = "labelPCInt";
      this.labelPCInt.Size = new System.Drawing.Size(44, 15);
      this.labelPCInt.TabIndex = 12;
      this.labelPCInt.Text = "label13";
      // 
      // labelPCLATH
      // 
      this.labelPCLATH.AutoSize = true;
      this.labelPCLATH.Location = new System.Drawing.Point(113, 105);
      this.labelPCLATH.Name = "labelPCLATH";
      this.labelPCLATH.Size = new System.Drawing.Size(44, 15);
      this.labelPCLATH.TabIndex = 11;
      this.labelPCLATH.Text = "label12";
      // 
      // labelPCL
      // 
      this.labelPCL.AutoSize = true;
      this.labelPCL.Location = new System.Drawing.Point(113, 70);
      this.labelPCL.Name = "labelPCL";
      this.labelPCL.Size = new System.Drawing.Size(44, 15);
      this.labelPCL.TabIndex = 10;
      this.labelPCL.Text = "label11";
      // 
      // labelW
      // 
      this.labelW.AutoSize = true;
      this.labelW.Location = new System.Drawing.Point(113, 35);
      this.labelW.Name = "labelW";
      this.labelW.Size = new System.Drawing.Size(44, 15);
      this.labelW.TabIndex = 9;
      this.labelW.Text = "label10";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(251, 105);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(46, 15);
      this.label9.TabIndex = 8;
      this.label9.Text = "Timer0:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(251, 70);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(53, 15);
      this.label8.TabIndex = 7;
      this.label8.Text = "Vorteiler:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(251, 34);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(47, 15);
      this.label7.TabIndex = 6;
      this.label7.Text = "Option:";
      this.label7.Click += new System.EventHandler(this.label7_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 210);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(26, 15);
      this.label6.TabIndex = 5;
      this.label6.Text = "FSR";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 175);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 15);
      this.label5.TabIndex = 4;
      this.label5.Text = "Status:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 140);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(59, 15);
      this.label4.TabIndex = 3;
      this.label4.Text = "PC intern:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 105);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 15);
      this.label3.TabIndex = 2;
      this.label3.Text = "PCLATH:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 70);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(31, 15);
      this.label2.TabIndex = 1;
      this.label2.Text = "PCL:";
      this.label2.Click += new System.EventHandler(this.label2_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 35);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(68, 15);
      this.label1.TabIndex = 0;
      this.label1.Text = "W-Register:";
      this.label1.Click += new System.EventHandler(this.label1_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label18);
      this.groupBox2.Controls.Add(this.label17);
      this.groupBox2.Controls.Add(this.dataGridViewIntCon);
      this.groupBox2.Controls.Add(this.dataGridViewOption);
      this.groupBox2.Controls.Add(this.label10);
      this.groupBox2.Controls.Add(this.dataGridViewStatus);
      this.groupBox2.Location = new System.Drawing.Point(422, 24);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(364, 257);
      this.groupBox2.TabIndex = 5;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "SFR";
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(20, 165);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(44, 15);
      this.label18.TabIndex = 5;
      this.label18.Text = "Intcon:";
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(20, 105);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(47, 15);
      this.label17.TabIndex = 4;
      this.label17.Text = "Option:";
      // 
      // dataGridViewIntCon
      // 
      this.dataGridViewIntCon.AllowUserToAddRows = false;
      this.dataGridViewIntCon.AllowUserToDeleteRows = false;
      this.dataGridViewIntCon.AllowUserToResizeColumns = false;
      this.dataGridViewIntCon.AllowUserToResizeRows = false;
      this.dataGridViewIntCon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewIntCon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
      this.dataGridViewIntCon.Location = new System.Drawing.Point(80, 165);
      this.dataGridViewIntCon.MultiSelect = false;
      this.dataGridViewIntCon.Name = "dataGridViewIntCon";
      this.dataGridViewIntCon.ReadOnly = true;
      this.dataGridViewIntCon.RowTemplate.Height = 25;
      this.dataGridViewIntCon.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.dataGridViewIntCon.Size = new System.Drawing.Size(285, 50);
      this.dataGridViewIntCon.TabIndex = 3;
      this.dataGridViewIntCon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick);
      // 
      // dataGridViewTextBoxColumn9
      // 
      this.dataGridViewTextBoxColumn9.HeaderText = "GIE";
      this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
      this.dataGridViewTextBoxColumn9.ReadOnly = true;
      this.dataGridViewTextBoxColumn9.Width = 30;
      // 
      // dataGridViewTextBoxColumn10
      // 
      this.dataGridViewTextBoxColumn10.HeaderText = "EIE";
      this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
      this.dataGridViewTextBoxColumn10.ReadOnly = true;
      this.dataGridViewTextBoxColumn10.Width = 30;
      // 
      // dataGridViewTextBoxColumn11
      // 
      this.dataGridViewTextBoxColumn11.HeaderText = "TIE";
      this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
      this.dataGridViewTextBoxColumn11.ReadOnly = true;
      this.dataGridViewTextBoxColumn11.Width = 30;
      // 
      // dataGridViewTextBoxColumn12
      // 
      this.dataGridViewTextBoxColumn12.HeaderText = "IE";
      this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
      this.dataGridViewTextBoxColumn12.ReadOnly = true;
      this.dataGridViewTextBoxColumn12.Width = 30;
      // 
      // dataGridViewTextBoxColumn13
      // 
      this.dataGridViewTextBoxColumn13.HeaderText = "RIE";
      this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
      this.dataGridViewTextBoxColumn13.ReadOnly = true;
      this.dataGridViewTextBoxColumn13.Width = 30;
      // 
      // dataGridViewTextBoxColumn14
      // 
      this.dataGridViewTextBoxColumn14.HeaderText = "TIF";
      this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
      this.dataGridViewTextBoxColumn14.ReadOnly = true;
      this.dataGridViewTextBoxColumn14.Width = 30;
      // 
      // dataGridViewTextBoxColumn15
      // 
      this.dataGridViewTextBoxColumn15.HeaderText = "IF";
      this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
      this.dataGridViewTextBoxColumn15.ReadOnly = true;
      this.dataGridViewTextBoxColumn15.Width = 30;
      // 
      // dataGridViewTextBoxColumn16
      // 
      this.dataGridViewTextBoxColumn16.HeaderText = "RIF";
      this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
      this.dataGridViewTextBoxColumn16.ReadOnly = true;
      this.dataGridViewTextBoxColumn16.Width = 30;
      // 
      // dataGridViewOption
      // 
      this.dataGridViewOption.AllowUserToAddRows = false;
      this.dataGridViewOption.AllowUserToDeleteRows = false;
      this.dataGridViewOption.AllowUserToResizeColumns = false;
      this.dataGridViewOption.AllowUserToResizeRows = false;
      this.dataGridViewOption.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewOption.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
      this.dataGridViewOption.Location = new System.Drawing.Point(80, 100);
      this.dataGridViewOption.MultiSelect = false;
      this.dataGridViewOption.Name = "dataGridViewOption";
      this.dataGridViewOption.ReadOnly = true;
      this.dataGridViewOption.RowTemplate.Height = 25;
      this.dataGridViewOption.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.dataGridViewOption.Size = new System.Drawing.Size(285, 50);
      this.dataGridViewOption.TabIndex = 2;
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.HeaderText = "RPU";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      this.dataGridViewTextBoxColumn1.Width = 30;
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.HeaderText = "IEg";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      this.dataGridViewTextBoxColumn2.ReadOnly = true;
      this.dataGridViewTextBoxColumn2.Width = 30;
      // 
      // dataGridViewTextBoxColumn3
      // 
      this.dataGridViewTextBoxColumn3.HeaderText = "TCs";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      this.dataGridViewTextBoxColumn3.ReadOnly = true;
      this.dataGridViewTextBoxColumn3.Width = 30;
      // 
      // dataGridViewTextBoxColumn4
      // 
      this.dataGridViewTextBoxColumn4.HeaderText = "TSe";
      this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
      this.dataGridViewTextBoxColumn4.ReadOnly = true;
      this.dataGridViewTextBoxColumn4.Width = 30;
      // 
      // dataGridViewTextBoxColumn5
      // 
      this.dataGridViewTextBoxColumn5.HeaderText = "PSA";
      this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
      this.dataGridViewTextBoxColumn5.ReadOnly = true;
      this.dataGridViewTextBoxColumn5.Width = 30;
      // 
      // dataGridViewTextBoxColumn6
      // 
      this.dataGridViewTextBoxColumn6.HeaderText = "PS2";
      this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
      this.dataGridViewTextBoxColumn6.ReadOnly = true;
      this.dataGridViewTextBoxColumn6.Width = 30;
      // 
      // dataGridViewTextBoxColumn7
      // 
      this.dataGridViewTextBoxColumn7.HeaderText = "PS1";
      this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
      this.dataGridViewTextBoxColumn7.ReadOnly = true;
      this.dataGridViewTextBoxColumn7.Width = 30;
      // 
      // dataGridViewTextBoxColumn8
      // 
      this.dataGridViewTextBoxColumn8.HeaderText = "PS0";
      this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
      this.dataGridViewTextBoxColumn8.ReadOnly = true;
      this.dataGridViewTextBoxColumn8.Width = 30;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(20, 35);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(42, 15);
      this.label10.TabIndex = 1;
      this.label10.Text = "Status:";
      // 
      // dataGridViewStatus
      // 
      this.dataGridViewStatus.AllowUserToAddRows = false;
      this.dataGridViewStatus.AllowUserToDeleteRows = false;
      this.dataGridViewStatus.AllowUserToResizeColumns = false;
      this.dataGridViewStatus.AllowUserToResizeRows = false;
      this.dataGridViewStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIRP,
            this.ColumnRP1,
            this.ColumnRP0,
            this.ColumnT0,
            this.ColumnPD,
            this.ColumnZero,
            this.ColumnDC,
            this.ColumnC});
      this.dataGridViewStatus.Location = new System.Drawing.Point(80, 35);
      this.dataGridViewStatus.MultiSelect = false;
      this.dataGridViewStatus.Name = "dataGridViewStatus";
      this.dataGridViewStatus.ReadOnly = true;
      this.dataGridViewStatus.RowTemplate.Height = 25;
      this.dataGridViewStatus.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.dataGridViewStatus.Size = new System.Drawing.Size(285, 50);
      this.dataGridViewStatus.TabIndex = 0;
      this.dataGridViewStatus.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStatus_CellClick);
      this.dataGridViewStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStatus_CellContentClick);
      // 
      // ColumnIRP
      // 
      this.ColumnIRP.HeaderText = "IRP";
      this.ColumnIRP.Name = "ColumnIRP";
      this.ColumnIRP.ReadOnly = true;
      this.ColumnIRP.Width = 30;
      // 
      // ColumnRP1
      // 
      this.ColumnRP1.HeaderText = "RP1";
      this.ColumnRP1.Name = "ColumnRP1";
      this.ColumnRP1.ReadOnly = true;
      this.ColumnRP1.Width = 30;
      // 
      // ColumnRP0
      // 
      this.ColumnRP0.HeaderText = "RP0";
      this.ColumnRP0.Name = "ColumnRP0";
      this.ColumnRP0.ReadOnly = true;
      this.ColumnRP0.Width = 30;
      // 
      // ColumnT0
      // 
      this.ColumnT0.HeaderText = "T0";
      this.ColumnT0.Name = "ColumnT0";
      this.ColumnT0.ReadOnly = true;
      this.ColumnT0.Width = 30;
      // 
      // ColumnPD
      // 
      this.ColumnPD.HeaderText = "PD";
      this.ColumnPD.Name = "ColumnPD";
      this.ColumnPD.ReadOnly = true;
      this.ColumnPD.Width = 30;
      // 
      // ColumnZero
      // 
      this.ColumnZero.HeaderText = "Z";
      this.ColumnZero.Name = "ColumnZero";
      this.ColumnZero.ReadOnly = true;
      this.ColumnZero.Width = 30;
      // 
      // ColumnDC
      // 
      this.ColumnDC.HeaderText = "DC";
      this.ColumnDC.Name = "ColumnDC";
      this.ColumnDC.ReadOnly = true;
      this.ColumnDC.Width = 30;
      // 
      // ColumnC
      // 
      this.ColumnC.HeaderText = "C";
      this.ColumnC.Name = "ColumnC";
      this.ColumnC.ReadOnly = true;
      this.ColumnC.Width = 30;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.labelS8);
      this.groupBox3.Controls.Add(this.labelS7);
      this.groupBox3.Controls.Add(this.labelS6);
      this.groupBox3.Controls.Add(this.labelS5);
      this.groupBox3.Controls.Add(this.labelS4);
      this.groupBox3.Controls.Add(this.labelS3);
      this.groupBox3.Controls.Add(this.labelS2);
      this.groupBox3.Controls.Add(this.labelS1);
      this.groupBox3.Location = new System.Drawing.Point(793, 25);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(155, 256);
      this.groupBox3.TabIndex = 6;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Stack";
      // 
      // labelS8
      // 
      this.labelS8.AutoSize = true;
      this.labelS8.Location = new System.Drawing.Point(50, 200);
      this.labelS8.Name = "labelS8";
      this.labelS8.Size = new System.Drawing.Size(44, 15);
      this.labelS8.TabIndex = 7;
      this.labelS8.Text = "label18";
      // 
      // labelS7
      // 
      this.labelS7.AutoSize = true;
      this.labelS7.Location = new System.Drawing.Point(50, 175);
      this.labelS7.Name = "labelS7";
      this.labelS7.Size = new System.Drawing.Size(44, 15);
      this.labelS7.TabIndex = 6;
      this.labelS7.Text = "label17";
      // 
      // labelS6
      // 
      this.labelS6.AutoSize = true;
      this.labelS6.Location = new System.Drawing.Point(50, 150);
      this.labelS6.Name = "labelS6";
      this.labelS6.Size = new System.Drawing.Size(44, 15);
      this.labelS6.TabIndex = 5;
      this.labelS6.Text = "label16";
      // 
      // labelS5
      // 
      this.labelS5.AutoSize = true;
      this.labelS5.Location = new System.Drawing.Point(50, 125);
      this.labelS5.Name = "labelS5";
      this.labelS5.Size = new System.Drawing.Size(44, 15);
      this.labelS5.TabIndex = 4;
      this.labelS5.Text = "label15";
      // 
      // labelS4
      // 
      this.labelS4.AutoSize = true;
      this.labelS4.Location = new System.Drawing.Point(50, 100);
      this.labelS4.Name = "labelS4";
      this.labelS4.Size = new System.Drawing.Size(44, 15);
      this.labelS4.TabIndex = 3;
      this.labelS4.Text = "label14";
      // 
      // labelS3
      // 
      this.labelS3.AutoSize = true;
      this.labelS3.Location = new System.Drawing.Point(50, 75);
      this.labelS3.Name = "labelS3";
      this.labelS3.Size = new System.Drawing.Size(44, 15);
      this.labelS3.TabIndex = 2;
      this.labelS3.Text = "label13";
      // 
      // labelS2
      // 
      this.labelS2.AutoSize = true;
      this.labelS2.Location = new System.Drawing.Point(50, 50);
      this.labelS2.Name = "labelS2";
      this.labelS2.Size = new System.Drawing.Size(44, 15);
      this.labelS2.TabIndex = 1;
      this.labelS2.Text = "label12";
      // 
      // labelS1
      // 
      this.labelS1.AutoSize = true;
      this.labelS1.Location = new System.Drawing.Point(50, 25);
      this.labelS1.Name = "labelS1";
      this.labelS1.Size = new System.Drawing.Size(44, 15);
      this.labelS1.TabIndex = 0;
      this.labelS1.Text = "label11";
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.buttonFileSelect);
      this.groupBox4.Controls.Add(this.Stopbtn);
      this.groupBox4.Controls.Add(this.Startbtn);
      this.groupBox4.Controls.Add(this.Resetbtn);
      this.groupBox4.Controls.Add(this.StartSimulation);
      this.groupBox4.Location = new System.Drawing.Point(959, 461);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(343, 256);
      this.groupBox4.TabIndex = 7;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Bedienelemente";
      // 
      // Stopbtn
      // 
      this.Stopbtn.Location = new System.Drawing.Point(12, 182);
      this.Stopbtn.Name = "Stopbtn";
      this.Stopbtn.Size = new System.Drawing.Size(75, 23);
      this.Stopbtn.TabIndex = 5;
      this.Stopbtn.Text = "Stop";
      this.Stopbtn.UseVisualStyleBackColor = true;
      this.Stopbtn.Click += new System.EventHandler(this.Stopbtn_Click);
      // 
      // Startbtn
      // 
      this.Startbtn.Location = new System.Drawing.Point(12, 136);
      this.Startbtn.Name = "Startbtn";
      this.Startbtn.Size = new System.Drawing.Size(75, 23);
      this.Startbtn.TabIndex = 4;
      this.Startbtn.Text = "Start";
      this.Startbtn.UseVisualStyleBackColor = true;
      this.Startbtn.Click += new System.EventHandler(this.Startbtn_Click);
      // 
      // Resetbtn
      // 
      this.Resetbtn.Location = new System.Drawing.Point(12, 47);
      this.Resetbtn.Name = "Resetbtn";
      this.Resetbtn.Size = new System.Drawing.Size(75, 23);
      this.Resetbtn.TabIndex = 3;
      this.Resetbtn.Text = "Reset";
      this.Resetbtn.UseVisualStyleBackColor = true;
      this.Resetbtn.Click += new System.EventHandler(this.Resetbtn_Click);
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.label16);
      this.groupBox5.Controls.Add(this.label15);
      this.groupBox5.Controls.Add(this.label14);
      this.groupBox5.Controls.Add(this.label13);
      this.groupBox5.Controls.Add(this.label12);
      this.groupBox5.Controls.Add(this.label11);
      this.groupBox5.Controls.Add(this.dataGridViewRB);
      this.groupBox5.Controls.Add(this.dataGridViewRA);
      this.groupBox5.Location = new System.Drawing.Point(954, 25);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(348, 256);
      this.groupBox5.TabIndex = 8;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "IO";
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(17, 200);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(26, 15);
      this.label16.TabIndex = 11;
      this.label16.Text = "PIN";
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(17, 86);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(26, 15);
      this.label15.TabIndex = 10;
      this.label15.Text = "PIN";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(17, 58);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(24, 15);
      this.label14.TabIndex = 9;
      this.label14.Text = "Tris";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(17, 174);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(24, 15);
      this.label13.TabIndex = 8;
      this.label13.Text = "Tris";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(17, 139);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(21, 15);
      this.label12.TabIndex = 7;
      this.label12.Text = "RB";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(17, 34);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(22, 15);
      this.label11.TabIndex = 6;
      this.label11.Text = "RA";
      // 
      // dataGridViewRB
      // 
      this.dataGridViewRB.AllowUserToAddRows = false;
      this.dataGridViewRB.AllowUserToDeleteRows = false;
      this.dataGridViewRB.AllowUserToResizeColumns = false;
      this.dataGridViewRB.AllowUserToResizeRows = false;
      this.dataGridViewRB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewRB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32});
      this.dataGridViewRB.Location = new System.Drawing.Point(49, 139);
      this.dataGridViewRB.MultiSelect = false;
      this.dataGridViewRB.Name = "dataGridViewRB";
      this.dataGridViewRB.ReadOnly = true;
      this.dataGridViewRB.RowTemplate.Height = 25;
      this.dataGridViewRB.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.dataGridViewRB.Size = new System.Drawing.Size(283, 74);
      this.dataGridViewRB.TabIndex = 5;
      this.dataGridViewRB.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRB_CellClick);
      // 
      // dataGridViewTextBoxColumn25
      // 
      this.dataGridViewTextBoxColumn25.HeaderText = "7";
      this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
      this.dataGridViewTextBoxColumn25.ReadOnly = true;
      this.dataGridViewTextBoxColumn25.Width = 30;
      // 
      // dataGridViewTextBoxColumn26
      // 
      this.dataGridViewTextBoxColumn26.HeaderText = "6";
      this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
      this.dataGridViewTextBoxColumn26.ReadOnly = true;
      this.dataGridViewTextBoxColumn26.Width = 30;
      // 
      // dataGridViewTextBoxColumn27
      // 
      this.dataGridViewTextBoxColumn27.HeaderText = "5";
      this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
      this.dataGridViewTextBoxColumn27.ReadOnly = true;
      this.dataGridViewTextBoxColumn27.Width = 30;
      // 
      // dataGridViewTextBoxColumn28
      // 
      this.dataGridViewTextBoxColumn28.HeaderText = "4";
      this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
      this.dataGridViewTextBoxColumn28.ReadOnly = true;
      this.dataGridViewTextBoxColumn28.Width = 30;
      // 
      // dataGridViewTextBoxColumn29
      // 
      this.dataGridViewTextBoxColumn29.HeaderText = "3";
      this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
      this.dataGridViewTextBoxColumn29.ReadOnly = true;
      this.dataGridViewTextBoxColumn29.Width = 30;
      // 
      // dataGridViewTextBoxColumn30
      // 
      this.dataGridViewTextBoxColumn30.HeaderText = "2";
      this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
      this.dataGridViewTextBoxColumn30.ReadOnly = true;
      this.dataGridViewTextBoxColumn30.Width = 30;
      // 
      // dataGridViewTextBoxColumn31
      // 
      this.dataGridViewTextBoxColumn31.HeaderText = "1";
      this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
      this.dataGridViewTextBoxColumn31.ReadOnly = true;
      this.dataGridViewTextBoxColumn31.Width = 30;
      // 
      // dataGridViewTextBoxColumn32
      // 
      this.dataGridViewTextBoxColumn32.HeaderText = "0";
      this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
      this.dataGridViewTextBoxColumn32.ReadOnly = true;
      this.dataGridViewTextBoxColumn32.Width = 30;
      // 
      // dataGridViewRA
      // 
      this.dataGridViewRA.AllowUserToAddRows = false;
      this.dataGridViewRA.AllowUserToDeleteRows = false;
      this.dataGridViewRA.AllowUserToResizeColumns = false;
      this.dataGridViewRA.AllowUserToResizeRows = false;
      this.dataGridViewRA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewRA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24});
      this.dataGridViewRA.Location = new System.Drawing.Point(49, 34);
      this.dataGridViewRA.MultiSelect = false;
      this.dataGridViewRA.Name = "dataGridViewRA";
      this.dataGridViewRA.ReadOnly = true;
      this.dataGridViewRA.RowTemplate.Height = 25;
      this.dataGridViewRA.ScrollBars = System.Windows.Forms.ScrollBars.None;
      this.dataGridViewRA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
      this.dataGridViewRA.Size = new System.Drawing.Size(283, 74);
      this.dataGridViewRA.TabIndex = 4;
      this.dataGridViewRA.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRA_CellClick);
      // 
      // dataGridViewTextBoxColumn17
      // 
      this.dataGridViewTextBoxColumn17.HeaderText = "7";
      this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
      this.dataGridViewTextBoxColumn17.ReadOnly = true;
      this.dataGridViewTextBoxColumn17.Width = 30;
      // 
      // dataGridViewTextBoxColumn18
      // 
      this.dataGridViewTextBoxColumn18.HeaderText = "6";
      this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
      this.dataGridViewTextBoxColumn18.ReadOnly = true;
      this.dataGridViewTextBoxColumn18.Width = 30;
      // 
      // dataGridViewTextBoxColumn19
      // 
      this.dataGridViewTextBoxColumn19.HeaderText = "5";
      this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
      this.dataGridViewTextBoxColumn19.ReadOnly = true;
      this.dataGridViewTextBoxColumn19.Width = 30;
      // 
      // dataGridViewTextBoxColumn20
      // 
      this.dataGridViewTextBoxColumn20.HeaderText = "4";
      this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
      this.dataGridViewTextBoxColumn20.ReadOnly = true;
      this.dataGridViewTextBoxColumn20.Width = 30;
      // 
      // dataGridViewTextBoxColumn21
      // 
      this.dataGridViewTextBoxColumn21.HeaderText = "3";
      this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
      this.dataGridViewTextBoxColumn21.ReadOnly = true;
      this.dataGridViewTextBoxColumn21.Width = 30;
      // 
      // dataGridViewTextBoxColumn22
      // 
      this.dataGridViewTextBoxColumn22.HeaderText = "2";
      this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
      this.dataGridViewTextBoxColumn22.ReadOnly = true;
      this.dataGridViewTextBoxColumn22.Width = 30;
      // 
      // dataGridViewTextBoxColumn23
      // 
      this.dataGridViewTextBoxColumn23.HeaderText = "1";
      this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
      this.dataGridViewTextBoxColumn23.ReadOnly = true;
      this.dataGridViewTextBoxColumn23.Width = 30;
      // 
      // dataGridViewTextBoxColumn24
      // 
      this.dataGridViewTextBoxColumn24.HeaderText = "0";
      this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
      this.dataGridViewTextBoxColumn24.ReadOnly = true;
      this.dataGridViewTextBoxColumn24.Width = 30;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(9, 45);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(74, 15);
      this.label19.TabIndex = 9;
      this.label19.Text = "Laufzeit in µ:";
      // 
      // labelLaufzeit
      // 
      this.labelLaufzeit.AutoSize = true;
      this.labelLaufzeit.Location = new System.Drawing.Point(107, 45);
      this.labelLaufzeit.Name = "labelLaufzeit";
      this.labelLaufzeit.Size = new System.Drawing.Size(44, 15);
      this.labelLaufzeit.TabIndex = 10;
      this.labelLaufzeit.Text = "label20";
      // 
      // comboBox1
      // 
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[] {
            "32 kHz",
            "100 kHz",
            "500 kHz",
            "1 MHz",
            "2 MHz",
            "4 MHz",
            "8 MHz",
            "12 MHz",
            "16 MHz",
            "20 MHz"});
      this.comboBox1.Location = new System.Drawing.Point(107, 95);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(121, 23);
      this.comboBox1.TabIndex = 11;
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(5, 103);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(87, 15);
      this.label21.TabIndex = 12;
      this.label21.Text = "Quarzfrequenz:";
      // 
      // Timing
      // 
      this.Timing.Controls.Add(this.comboBox1);
      this.Timing.Controls.Add(this.label19);
      this.Timing.Controls.Add(this.labelLaufzeit);
      this.Timing.Controls.Add(this.label21);
      this.Timing.Location = new System.Drawing.Point(954, 287);
      this.Timing.Name = "Timing";
      this.Timing.Size = new System.Drawing.Size(348, 168);
      this.Timing.TabIndex = 13;
      this.Timing.TabStop = false;
      this.Timing.Text = "Timing";
      // 
      // openFileDialog1
      // 
      this.openFileDialog1.FileName = "openFileDialog1";
      // 
      // buttonFileSelect
      // 
      this.buttonFileSelect.Location = new System.Drawing.Point(172, 47);
      this.buttonFileSelect.Name = "buttonFileSelect";
      this.buttonFileSelect.Size = new System.Drawing.Size(110, 23);
      this.buttonFileSelect.TabIndex = 6;
      this.buttonFileSelect.Text = "Datei öffnen";
      this.buttonFileSelect.UseVisualStyleBackColor = true;
      this.buttonFileSelect.Click += new System.EventHandler(this.buttonFileSelect_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1751, 915);
      this.Controls.Add(this.Timing);
      this.Controls.Add(this.groupBox5);
      this.Controls.Add(this.groupBox4);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.dataGridView2);
      this.Controls.Add(this.dataGridView1);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIntCon)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOption)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatus)).EndInit();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRA)).EndInit();
      this.Timing.ResumeLayout(false);
      this.Timing.PerformLayout();
      this.ResumeLayout(false);

    }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button StartSimulation;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Timer cycle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridView dataGridView2;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label labelTimer0;
    private System.Windows.Forms.Label labelVorteiler;
    private System.Windows.Forms.Label labelOption;
    private System.Windows.Forms.Label labelFSR;
    private System.Windows.Forms.Label labelStatus;
    private System.Windows.Forms.Label labelPCInt;
    private System.Windows.Forms.Label labelPCLATH;
    private System.Windows.Forms.Label labelPCL;
    private System.Windows.Forms.Label labelW;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label labelTimerZero;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.DataGridView dataGridViewStatus;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIRP;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRP1;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRP0;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnT0;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPD;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnZero;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDC;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnC;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label labelS8;
    private System.Windows.Forms.Label labelS7;
    private System.Windows.Forms.Label labelS6;
    private System.Windows.Forms.Label labelS5;
    private System.Windows.Forms.Label labelS4;
    private System.Windows.Forms.Label labelS3;
    private System.Windows.Forms.Label labelS2;
    private System.Windows.Forms.Label labelS1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button Stopbtn;
        private System.Windows.Forms.Button Startbtn;
        private System.Windows.Forms.Button Resetbtn;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.DataGridView dataGridView6;
    private System.Windows.Forms.DataGridView dataGridViewRA;
    private System.Windows.Forms.DataGridView dataGridViewRB;
        private System.Windows.Forms.DataGridViewTextBoxColumn Breakpoint;
        private System.Windows.Forms.DataGridViewTextBoxColumn Header;
        private System.Windows.Forms.DataGridViewTextBoxColumn PCL;
    private System.Windows.Forms.DataGridView dataGridViewOption;
    private System.Windows.Forms.DataGridView dataGridView4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    private System.Windows.Forms.DataGridView dataGridViewIntCon;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Label labelLaufzeit;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.GroupBox Timing;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
    private System.Windows.Forms.Button buttonFileSelect;
    private System.Windows.Forms.OpenFileDialog openFileDialog1;
  }
}

