﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIMP
{
  class Fetcher
  {



    public string[] file;
    int[] programmBuffer;


    public Fetcher()
    {
      
    }

    public void setprogrammbuffer(string filename)
    {
      file = System.IO.File.ReadAllLines(filename);
      programmBuffer = new int[2 * (file.Length)];

      int counter = 0;
      int i = 0;
      for (i = 0; i < file.Length; i++)
      {
        if (!file[i].StartsWith(" "))
        {
          string op = Convert.ToString(file[i].Substring(5, 4));
          programmBuffer[counter++] = Convert.ToInt32(op, 16);
        }
      }



    }

    public int getcommand(byte pcl)
    {
      return programmBuffer[pcl];
    }
  }
}
